<?php

function tree_to_menu_select($list,$selectedid='',$level = 0,$repeat = "&nbsp;--")
{
   echo $level."--";
   $data = '';

    foreach ($list as $key => $value) {
        if($value['id'] == $selectedid){
                $selected="selected='selected'";
        $data = $data."<option value='".$value['pid']."' $selected>".str_repeat($repeat, $level).$value['name']."</option>";
        }else{ 
            $data = $data."<option value='".$value['id']."'>".str_repeat($repeat, $level).$value['name']."</option>";
        }
        if (!empty($value['_child'])) {
            $data = $data.tree_to_menu_select($value['_child'],$selectedid,$level+1);
        }
    }
   
    return $data;
}
/**
 * 把返回的数据集转换成Tree
 * @param array $list 要转换的数据集
 * @param string $pid parent标记字段
 * @param string $level level标记字段
 * @return array

 */
function list_to_tree($list, $pk='id', $pid = 'pid', $child = '_child', $root = 0) {
    // 创建Tree
    $tree = array();
    if(is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId =  $data[$pid];
            if ($root == $parentId) {
                $tree[] =& $list[$key];
            }else{
                if (isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    return $tree;
}
//封装成一个数组
$arr=array(  
array('id'=>'1','name'=>'北京','pid'=>'0'),  
array('id'=>'2','name'=>'上海','pid'=>'1'),  
array('id'=>'3','name'=>'浦东','pid'=>'0'),  
array('id'=>'4','name'=>'朝阳','pid'=>'0'),  
array('id'=>'5','name'=>'广州','pid'=>'0'),  
array('id'=>'6','name'=>'三里屯','pid'=>'0'),  
array('id'=>'7','name'=>'广东','pid'=>'0'),  
array('id'=>'8','name'=>'三里','pid'=>'0') 
) ; 

$arr=list_to_tree($arr);
?>
 <select class="form-control" name="pid">
    <option value="0">无</option>
                   <?php echo tree_to_menu_select($arr)?>
   </select>